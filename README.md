# package_alerts

* This is a script that parses Red Hat API  content from [https://access.redhat.com/management/api/rhsm](https://access.redhat.com/management/api/rhsm) and stores the content in a text file
* Should there be a package that is deemed as interesting, the script will make a post to a mattermost integration url

* Neccessary variables:
  * `SCHEDULE`: Nomad schedule
  * `DB_MOUNTPOINT`: Filesystem path to mount as `/dbmount``
  * `FEEDS`: Comma separated list of "csets" to check from Red Hat
  * `INTERESTING_PACKAGES`: Name of the file that contains info about the packages we should inform about via mattermost
  * `MATTERMOST_INTEGRATION_URL_PATH`: A path that contains the mattermost integration url used for alerting

Most modifications will be done to `/package_alerts/prod.packages.yml`, which is where you can set up
what packages you want to keep an eye on. You can add a specific feed to watch and notes to include
in the notification, like so:

```
---
packages:
  glog:
    feed: 'rhel-9-for-x86_64-baseos-rpms'
    notes: |-
      Hopefully this package fixes [Bugzilla 2055222](https://bugzilla.redhat.com/show_bug.cgi?id=2055222).
      If it does, then you can remove `gflags-2.2.2-1.el8.*` from [stream8_snapshots/packages_filtered.lst](https://gitlab.cern.ch/linuxsupport/cronjobs/stream8_snapshots/-/blob/161bdc37a36f103881e66ce7e84fe046a8720cea/stream8_snapshots/packages_filtered.lst#L14).
```
