job "${PREFIX}_package_alerts" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_package_alerts" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/package_alerts/package_alerts:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_package_alerts"
        }
      }
      volumes = [
        "${DB_MOUNTPOINT}:/work",
        "${MATTERMOST_INTEGRATION_URL_PATH}:/etc/mattermost_hook_url",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      INTERESTING_PACKAGES = "$INTERESTING_PACKAGES"
      FEEDS = "$FEEDS"
      RHSM_OFFLINE_TOKEN = "$RHSM_OFFLINE_TOKEN"
      TAG = "${PREFIX}_package_alerts"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 1024 # MB
    }

  }
}

